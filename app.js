const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');

const app = express();

const PAGE_ACCESS_TOKEN = 'DQWRLeDgzQ2hUZAml6emFDZA2hkVXpmREZAJd3FSN2FpRWlwUjdqY01mZAlRnREFKUUctRGlETm8wd05LRF9WMVM1TnpxSDNfMG1ibkdxM01yeE5vUlBGbzJWMElqRTMwYXNpS0sweU1CYWlCXzN1WGlJem5yeWpSZAVQ3RFFOWGlFX19wRVFwYy0wZAUdGa3FwMXdUQzg2aVBkVjlVdFlvLVdPckY3N19PTHdqeE5wblV4LS12Q0c2blNuREVrY040UzZA0VWxOZADBWR0tpTnFpbGwtX1hn'; // Get this token from your Facebook App.

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello, Chatbot!');
});

app.post('/webhook', (req, res) => {
  const data = req.body;

  if (data.object === 'page') {
    data.entry.forEach(entry => {
      const webhookEvent = entry.messaging[0];
      {
  "recipient": {"ids": ["100074679903861"]},
  "message": {
    "attachment": {
	    "type": "template",
	    "payload": {
	      "template_type": "button",
	      "text": "This is test text",
	      "buttons":[
	      	{
	    		"type": "web_url",
	    		"url": "https://www.oculus.com/en-us/rift/",
	    		"title": "Open Web URL"
	    	}, 
		    {
		      "type": "postback",
		      "title": "Trigger Postback",
		      "payload": "postback payload"
		    }, 
		    {
		      "type": "phone_number",
		      "title": "Call Phone Number",
		      "payload": "1234567890"
		     }
		     ]
	    }
    }
  }
}
    });
  }

  res.sendStatus(200);
});

app.listen(process.env.PORT || 3000, () => {
  console.log('Chatbot is running on port 3000 (or the port specified by your hosting platform).');
});
